<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/webhook', function(Request $req){

    \Illuminate\Support\Facades\Log::info($req->input());

});

Route::get('/webhook', function(Request $req){


    $query = $req->query();

    $apptoken = '12345678';

    $mode = $req->query('hub_mode');

    $token = $req->query('hub_verify_token');

    $challenge = $req->query('hub_challenge');


    

    if($mode && $token){

        if($mode == "subscribe" && $token == $apptoken){
            return response($challenge, 200);
        }

    }else{
        return response("", 403);
    }



});

